package Book;

public class BookColection {

    public static void main(String[] args) {
        Book rowling = new Book(1, "Harry Potter and the Philosopher's Stone", "Rowling", "Bloomsbury", 1997, 223, 85, "hard");
        Book orwell = new Book(2, "1984", "Orwell", "Secker & Warburg", 1949, 328, 60, "hard");
        Book tolkien = new Book(3, "The Lord of the Rings", "Tolkien", "George Allen & Unwin", 1954, 1137, 260, "hard");
        Book hemingway = new Book(4, "The Old Man and the Sea", "Hemingway", "Charles Scribner's Sons", 1952, 127, 55, "soft");
        Book fitzgerald = new Book(5, "The Great Gatsby", "Fitzgerald", "Charles Scribner's Sons", 1925, 180, 55, "soft");

        Library myBooks = new Library(new Book[]{rowling, orwell, tolkien, hemingway});
        System.out.println("All books in the library: ");
        for (Book book : myBooks.getContent()) {
            System.out.println(book);
        }
        System.out.println();

        myBooks.add(fitzgerald);

        System.out.println("Books by Orwell: ");
        for (Book book : myBooks.findByAuthor("Orwell")) {
            System.out.println(book);
        }
        System.out.println();

        System.out.println("Books published by Charles Scribner's Sons: ");
        for (Book book : myBooks.findByPublisher("Charles Scribner's Sons")) {
            System.out.println(book);
        }
        System.out.println();

        System.out.println("Books published before 1952: ");
        for (Book book : myBooks.findYoungerThan(1952)) {
            System.out.println(book);
        }
    }
}

