package Customer;

import java.util.List;

public class Custom {
    public static void main(String[] args) {
        CustomerList list = new CustomerList();
        list.addCustomer(new Customer(1, "Smith", "John", "", "123 Main St", "1234567890123456", "9876543210987654", "john.smith@email.com", "555-555-5555"));
        list.addCustomer(new Customer(2, "Johnson", "Jane", "", "456 Park Ave", "2345678901234567", "8765432109876543", "jane.johnson@email.com", "555-555-5556"));
        list.addCustomer(new Customer(3, "Williams", "Bob", "", "789 Elm St", "3456789012345678", "7654321098765432", "bob.williams@email.com", "555-555-5557"));

        list.sortBySurname();
        System.out.println("Customers sorted by surname:");
        for (Customer customer : list.customers) {
            System.out.println(customer.toString());
        }

        List<Customer> filteredCustomers = list.getCustomersInCreditCardRange("2345678901234567", "3456789012345678");
        System.out.println("\nCustomers with credit card numbers in range:");
        for (Customer customer : filteredCustomers) {
            System.out.println(customer.toString());
        }
    }
}
