package Book;
import java.util.Arrays;

public class Library {
    private Book[] content;
    private int size;

    public Library(Book[] content) {
        this.content = Arrays.copyOf(content, content.length);
        this.size = content.length;
    }

    public Book[] getContent() {
        return Arrays.copyOf(this.content, this.size);
    }

    public int getSize() {
        return size;
    }

    public void add(Book book) {
        for (int i = 0; i < this.size; i++) {
            if (this.content[i].equals(book)) {
                System.out.println("Already in the library.");
                return;
            }
        }

        if (this.size == this.content.length) {
            this.content = Arrays.copyOf(this.content, this.content.length + 10);
        }
        this.content[this.size] = book;
        this.size++;
    }

    public Book[] findByAuthor(String author) {
        Book[] result = new Book[this.size];
        int items = 0;

        for (int i = 0; i < this.size; i++) {
            if (this.content[i].getAuthor().equals(author)) {
                result[items] = this.content[i];
                items++;
            }
        }

        return Arrays.copyOf(result, items);
    }

    public Book[] findByPublisher(String publisher) {
        Book[] result = new Book[this.size];
        int items = 0;

        for (int i = 0; i < this.size; i++) {
            if (this.content[i].getPublisher().equals(publisher)) {
                result[items] = this.content[i];
                items++;
            }
        }

        return Arrays.copyOf(result, items);
    }

    public Book[] findYoungerThan(int year) {
        Book[] result = new Book[this.size];
        int items = 0;

        for (int i = 0; i < this.size; i++) {
            if (this.content[i].getYear() > year) {
                result[items] = this.content[i];
                items++;
            }
        }

        return Arrays.copyOf(result, items);
    }
}
