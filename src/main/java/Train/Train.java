package Train;

import java.util.Arrays;
import java.util.Scanner;

class Train {
    String destinationName;
    int trainNumber;
    String departureTime;

    public Train(String destinationName, int trainNumber, String departureTime) {
        this.destinationName = destinationName;
        this.trainNumber = trainNumber;
        this.departureTime = departureTime;
    }
}

