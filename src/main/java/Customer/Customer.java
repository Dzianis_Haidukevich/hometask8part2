package Customer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class Customer {
    private int id;
    private String surname;
    private String firstName;
    private String patronymic;
    private String address;
    private String creditCardNumber;
    private String bankAccountNumber;
    private String email;
    private String phoneNumber;

    public Customer() {
        // default constructor
    }

    public Customer(int id, String surname, String firstName, String patronymic, String address, String creditCardNumber, String bankAccountNumber, String email, String phoneNumber) {
        this.id = id;
        this.surname = surname;
        this.firstName = firstName;
        this.patronymic = patronymic;
        this.address = address;
        this.creditCardNumber = creditCardNumber;
        this.bankAccountNumber = bankAccountNumber;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "id: " + id + ", surname: " + surname + ", first name: " + firstName + ", patronymic: " + patronymic +
                ", address: " + address + ", credit card number: " + creditCardNumber + ", bank account number: " + bankAccountNumber +
                ", email: " + email + ", phone number: " + phoneNumber;
    }
}

class CustomerList {
    public List<Customer> customers;

    public CustomerList() {
        customers = new ArrayList<>();
    }

    public void addCustomer(Customer customer) {
        customers.add(customer);
    }

    public List<Customer> getCustomersInCreditCardRange(String startCardNumber, String endCardNumber) {
        List<Customer> filteredCustomers = new ArrayList<>();
        for (Customer customer : customers) {
            if (customer.getCreditCardNumber().compareTo(startCardNumber) >= 0 &&
                    customer.getCreditCardNumber().compareTo(endCardNumber) <= 0) {
                filteredCustomers.add(customer);
            }
        }
        return filteredCustomers;
    }

    public void sortBySurname() {
        Collections.sort(customers, new Comparator<Customer>() {
            public int compare(Customer c1, Customer c2) {
                return c1.getSurname().compareTo(c2.getSurname());
            }
        });
    }
}

