package Train;

import java.util.Arrays;
import java.util.Scanner;

public class Trains {
    public static void main(String[] args) {
        Train[] trains = new Train[5];
        trains[0] = new Train("New York", 1, "9:00am");
        trains[1] = new Train("Chicago", 2, "10:00am");
        trains[2] = new Train("Los Angeles", 3, "11:00am");
        trains[3] = new Train("New York", 4, "12:00pm");
        trains[4] = new Train("Chicago", 5, "1:00pm");

        // Sort the array by train number
        Arrays.sort(trains, (t1, t2) -> t1.trainNumber - t2.trainNumber);

        // Display information about the train whose number is entered by the user
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the train number: ");
        int trainNumber = scanner.nextInt();
        for (Train train : trains) {
            if (train.trainNumber == trainNumber) {
                System.out.println("Destination: " + train.destinationName);
                System.out.println("Departure time: " + train.departureTime);
                break;
            }
        }

        // Sort the array by destination name and departure time
        Arrays.sort(trains, (t1, t2) -> {
            if (t1.destinationName.compareTo(t2.destinationName) == 0) {
                return t1.departureTime.compareTo(t2.departureTime);
            }
            return t1.destinationName.compareTo(t2.destinationName);
        });
    }
}
