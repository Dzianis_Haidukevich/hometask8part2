package Book;

public class Seal {

    public static void printLibrary(Library library) {
        for (Book book : library.getContent()) {
            printBook(book);
        }
    }

    public static void printBooks(Book[] booksArray) {
        for (Book book : booksArray) {
            printBook(book);
        }
    }

    public static void printBook(Book book) {
        System.out.println(book);
    }
}

