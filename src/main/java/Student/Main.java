package Student;
class Student {
        String surname;
        String initials;
        int groupNumber;
        int[] academicPerformance;

public Student(String surname, String initials, int groupNumber, int[] academicPerformance) {
        this.surname = surname;
        this.initials = initials;
        this.groupNumber = groupNumber;
        this.academicPerformance = academicPerformance;
        }
        }

public class Main {
    public static void main(String[] args) {
        Student[] students = new Student[10];
        students[0] = new Student("John", "Doe", 1, new int[]{9, 8, 10, 9, 10});
        students[1] = new Student("Jane", "Smith", 2, new int[]{7, 6, 8, 7, 9});
        // Additional code to initialize the remaining elements of the students array

        for (Student student : students) {
            for (int grade : student.academicPerformance) {
                if (grade == 9 || grade == 10) {
                    System.out.println("Name: " + student.surname + " " + student.initials);
                    System.out.println("Group number: " + student.groupNumber);
                    break;
                }
            }
        }
    }
}
